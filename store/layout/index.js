import Settings from '@/components/layout/functions/Settings.vue'
import StackOverflow from '@/components/layout/functions/StackOverflow.vue'
import Quora from '@/components/layout/functions/Quora.vue'
import RSS from '@/components/layout/functions/RSS.vue'
import Reddit from '@/components/layout/functions/Reddit.vue'
import Git from '@/components/layout/functions/Git.vue'
import Pad from '@/components/layout/functions/Pad.vue'
import Sticky from '@/components/layout/functions/Sticky.vue'
import Playground from '@/components/layout/functions/Playground.vue'

export const state = () => ({
    mainSidebar: {
        miniVariant: true,
        left: true,
        state: true,
    },
    functionSidebars: [
        {
            id: 0,
            title: 'Settings',
            icon: 'fas fa-cog',
            state: false,
            component: Settings
        },
        {
            id: 1,
            title: 'Stack Overflow',
            icon: 'fab fa-stack-overflow',
            state: false,
            component: StackOverflow
        },
        {
            id: 2,
            title: 'Quora',
            icon: 'fab fa-quora',
            state: false,
            component: Quora
        },
        {
            id: 3,
            title: 'RSS',
            icon: 'fas fa-rss',
            state: false,
            component: RSS
        },
        {
            id: 4,
            title: 'Reddit',
            icon: 'fab fa-reddit-alien',
            state: false,
            component: Reddit
        },
        {
            id: 5,
            title: 'Git',
            icon: 'fab fa-git-square',
            state: false,
            component: Git
        },
        {
            id: 6,
            title: 'Pad',
            icon: 'fas fa-pen-alt',
            state: false,
            component: Pad
        },
        {
            id: 7,
            title: 'Sticky',
            icon: 'fas fa-sticky-note',
            state: false,
            component: Sticky
        },
        {
            id: 8,
            title: 'Playground',
            icon: 'fas fa-code',
            state: false,
            component: Playground
        }
    ],
    chatSidebar: {
        right: true,
        state: false
    }
})

export const mutations = {
    updateMainSidebarMiniVariant (state) {
        state.mainSidebar.miniVariant = !state.mainSidebar.miniVariant
    },
    updateChatState (state) {
        state.chatSidebar.state = !state.chatSidebar.state
    },
    updateFunctionSidebarState (state, id) {
        state.functionSidebars[id].state = !state.functionSidebars[id].state
    }
}

export const actions = {
}